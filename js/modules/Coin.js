export default class Coin {
  constructor(x, y, coinObj, coin_width, coin_height, dy) {
    this.x = x // top left x
    this.y = y // top left y
    this.coinObj = coinObj
    this.coin_width = coin_width
    this.coin_height = coin_height
    this.dy = dy
  }

  draw() {
    ctx.beginPath()
    ctx.drawImage(this.coinObj, this.coin_width*currentcoinframe , 0, this.coin_width, this.coin_height, this.x, this.y, this.coin_width, this.coin_height)
    ctx.fill()
    ctx.closePath()
  }

  update() {
    this.dy -= 0.29

    this.y += this.dy

    this.draw()
  }
}