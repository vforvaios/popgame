export default class Fish {
  constructor(x, y, dx, type, fishObj, fish_width, fish_height) {
    this.x = x // top left x
    this.y = y // top left y
    this.dx = dx // x velocity
    this.type = type
    this.fishObj = fishObj
    this.fish_width = fish_width
    this.fish_height = fish_height
  }

  draw() {
    ctx.beginPath();
    ctx.drawImage(this.fishObj, this.fish_width*currentframe , 0, this.fish_width, this.fish_height, this.x, this.y, this.fish_width, this.fish_height)
    ctx.fill();
  }

  update() {
    this.type === 0
      ? this.x -= this.dx
      : this.x += this.dx

    this.draw()
  }
}