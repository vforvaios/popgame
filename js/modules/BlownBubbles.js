import Bubble from './Bubble'

export default class BlownBubbles {
  constructor(x, y, r, dx, dy, colour) {
    this.x = x // top left x
    this.y = y // top left y
    this.r = r
    this.dx = dx // x velocity
    this.dy = dy // y velocity
    this.colour = colour
  }

  draw() {
    ctx.beginPath();
    ctx.fillStyle = this.colour
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI, true);
    ctx.fill();
  }

  update() {
    this.x += this.dx

    this.dy -= gravity;// gravity
    this.y += this.dy;
    this.dy *= friction;// friction

    this.draw() 
  }
}