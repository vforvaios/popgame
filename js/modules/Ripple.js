export default class Ripple {
  constructor(x, y, r, opacity) {
    this.x = x
    this.y = y
    this.r = r
    this.opacity = opacity
  }

  draw() {
    ctx.beginPath()
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI)
    ctx.strokeStyle = `rgba(193, 246, 254, ${this.opacity})`
    ctx.lineWidth = 2
    ctx.stroke()
  }

  update() {
    this.r += 0.4*radiusFriction
    this.opacity += 0.02*radiusFriction

    if (this.r >= water_ripple_vanish_radius) {
      this.opacity -= .12
    }
    
    this.draw()
  }
}