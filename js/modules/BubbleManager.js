import { getRandomInt } from './utilities'
import BlownBubbles from './BlownBubbles'
import ScoreDisplayed from './ScoreDisplayed'

export const BubbleManager = {
  resizeBubbles: function() {
    if (window.innerWidth <=1024) {
      minRadius = 20
      maxRadius = 30
    } else {
      minRadius = 16
      maxRadius = 25
    }
  },

  removeUnseenBlownBubbles: function() {
    let remainingBlownBubbles = blownbubbles.filter( b => b.y + b.r >= 0 )
    blownbubbles = [...remainingBlownBubbles]
  },

  removeBubbleScores: function() {
    let remainingBubbleScore = scores_per_bubble.filter( bs => bs.opacity > 0 )
    scores_per_bubble = [...remainingBubbleScore]
  },

  iterateBlownBubbles: function() {
    for (let i=0; i<blownbubbles.length; i++) {
      blownbubbles[i].update()
    }
  },

  iterateBubbles: function() {
    for (let i=0; i<bubbles.length; i++) {
      bubbles[i].update(animatedFishes)
    }
  },

  iterateSeaBubbles: function() {
    for (let i=0; i<sea_bubbles.length; i++) {
      sea_bubbles[i].update()
    }
  },

  removeUnseenSeaBubbles: function() {
    let remainingSeaBubbles = sea_bubbles.filter( b => b.y + b.r >= 0 )
    sea_bubbles = [...remainingSeaBubbles]
  },

  generateBlownBubbles: function(clickX, clickY, colour) {
    for (let i=0; i<3; i++) {
      blownbubble = new BlownBubbles(clickX, clickY, getRandomInt(2, 4), getRandomInt(-5, 5), getRandomInt(3, 5), colour)
      blownbubbles = [...blownbubbles, blownbubble]
    }
  },

  handleCollision: function(x, y) {
    BubbleManager.generateBlownBubbles(x, y, 'lavender')
        
    let newBubbles = bubbles.filter( b => {
      let dx = x - b.x
      let dy = y - b.y
      let dist = Math.abs(Math.sqrt(dx*dx + dy*dy));
      return dist > b.r
    } )
    bubbles = [...newBubbles]
    
    players_score -= fish_blown_score
    
    if (players_score <= 0) {
      players_score = 0
    }
    
    score_per_bubble = new ScoreDisplayed(`-${fish_blown_score}`, 9, x, y, .5, 1, .6, 'red')
    scores_per_bubble.push(score_per_bubble)
    if (playSounds) {
      blop.play()
    }
  }
}