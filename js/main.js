import Bubble from './modules/Bubble'
import Fish from './modules/Fish'
import ScoreDisplayed from './modules/ScoreDisplayed'
import LevelUpIndicator from './modules/LevelUpIndicator';
import Ripple from './modules/Ripple'
import SeaBubble from './modules/SeaBubble'
import Coin from './modules/Coin'
import { BubbleManager } from './modules/BubbleManager'
import {
  getRandomInt,
  resizeCanvas,
  updateLiteralsOnScreen,
  updateLivesOnScreen,
  firebaseConfig,
  propComparator,
  getRandomIntIncludingZero
} from './modules/utilities'
import * as firebase from "firebase/app"
import "firebase/database"

const CLICKLISTENER = 'desktop'
const TOUCHLISTENER = 'mobile'
const bootScene = document.querySelector('.boot-scene')
const gameScene = document.querySelector('.game-scene')
const losingScene = document.querySelector('.losing-scene')
const startButton = document.querySelector('.start-button')
const leaderboardButton = document.querySelector('.leaderboard-button')
const restartButton = document.querySelector('.play-again')
let countdownspan = document.querySelector('.countdown-span')
let finalScore = document.querySelector('.final-score')
let leaderboardWrapper = document.querySelector('.leaderboard-wrapper')
let leaderboardPopup = document.querySelector('.leaderboard-popup')
let leaderboardList = document.querySelector('.leaderboard-list')
let closeLeaderboardPopup = document.querySelector('.close-leaderboard-popup')
let sendScore = document.querySelector('.send-score')
let nicknameText = document.querySelector('.nickname-text')
let backButton = document.querySelector('.back-button')
let loadingWrapper = document.querySelector('.loading-wrapper')
let muteButton = document.querySelector('.mute-button')
let ws = document.querySelector('.watersound')

let BubblePop = {
  missedBalls: 0,
  playerLost: false,
  sentScoreToLeaderboard: false,
  missedHits: 0,
  
  resetState: function() {
    canvas.style.background = "url('./images/underwater-1.jpg')"
    BubblePop.missedBalls = 0
    BubblePop.playerLost = false
    BubblePop.sentScoreToLeaderboard = false
    BubblePop.missedHits = 0

    bubble
    bubbles = []
    min_dx = -1
    max_dx = 1
    min_dy = 1
    max_dy = 2
    quickBubbleLevel = 150
    nextBubble = 150
    blownbubble
    blownbubbles = []
    gravity = .85
    friction = 1.01
    players_score = 0
    bubble_score = 20
    spawnY = 100
    clearTimeout(timeout)
    levels = [
      { score: 800, generate: 130, spawnY: 90, min_dx: -2, max_dx: 2, min_dy: 2, max_dy: 4, pointsPerBall: 30, bg: 'underwater-2.jpg', nextFish: 250, quickNextFish: 250, nextCoin: 400, quickNextCoin: 400, coinvelocity: 14 },
      { score: 2500, generate: 110, spawnY: 85, min_dx: -3, max_dx: 3, min_dy: 3, max_dy: 5, pointsPerBall: 50, bg: 'underwater-3.jpg', nextFish: 200, quickNextFish: 200, nextCoin: 300, quickNextCoin: 300, coinvelocity: 14 },
      { score: 4500, generate: 95, spawnY: 80, min_dx: -3, max_dx: 3, min_dy: 4, max_dy: 6, pointsPerBall: 70, bg: 'underwater-6.jpg', nextFish: 180, quickNextFish: 180, nextCoin: 250, quickNextCoin: 250, coinvelocity: 14 },
      { score: 8000, generate: 85, spawnY: 75, min_dx: -4, max_dx: 4, min_dy: 5, max_dy: 7, pointsPerBall: 90, bg: 'underwater-4.jpg', nextFish: 160, quickNextFish: 160, nextCoin: 200, quickNextCoin: 200, coinvelocity: 14 },
      { score: 14000, generate: 75, spawnY: 60, min_dx: -4, max_dx: 4, min_dy: 7, max_dy: 9, pointsPerBall: 100, bg: 'underwater-5.jpg', nextFish: 140, quickNextFish: 140, nextCoin: 150, quickNextCoin: 150, coinvelocity: 14 }
    ]
    maxBallsMissed = 3
    fish = new Image()
    coins = new Image()
    currentframe = 4
    fishframenow
    fishframedesired
    fishframerate = 150
    fishframes = 7
    animatedFishes = []
    nextFish = 300
    quickNextFish = 300
    fish_min_dx = 2
    fish_max_dx = 3
    randomFish
    fishType
    fish_x
    fish_y
    randomDX
    radiusFriction = 1.01
    water_ripple_vanish_radius = 22
    water_ripple
    water_ripples = []
    penalty_score = 10
    score_per_bubble
    scores_per_bubble = []
    levelUpIndicatorText
    levelUpIndicators = []
    min_distance = 40
    max_distance = 70
    sea_bubble
    sea_bubbles = []
    quickSeaBubble = 800
    nextSeaBubble = 800
    inARowAchievement = 7
    inARow = 0
    inARowAchievementScore = 40
    fish_blown_bubble = false
    fish_blown_score = 100

    /* COINS VARIABLES -------------------- */
    currentcoinframe = 0
    coinframenow
    coinframedesired
    coinframes = 7
    coinframerate = 100
    coinwidth = 56
    coinheight = 56
    nextCoin = 600
    quickNextCoin = 600
    coin_x
    coin_y
    animatedCoin
    animatedCoins = []
    coinScore = 70
    coingravity = .25
    coinvelocity = 14
    
    BubbleManager.resizeBubbles()
  },

  init: function() {
   
    countdownspan.classList.add('visible')
    BubblePop.countDown(3).then( message => {
      countdownspan.classList.remove('visible')
      BubblePop.missedHits = 0
      fishframenow = Math.floor(Date.now());
      coinframenow = Math.floor(Date.now());

      fish.onload = function() {
        ctx.drawImage(fish, 0, 0);
      };
      fish.src = './images/fish2.png';

      fish2.onload = function() {
        ctx.drawImage(fish2, 0, 0);
      };
      fish2.src = './images/fish3.png';

      coins.onload = function() {
        ctx.drawImage(coins, 0, 0);
      };
      coins.src = './images/coins.png';
      BubblePop.animateGame()
    } )
    
  },

  initFirebase: function() {
    firebase.initializeApp(firebaseConfig)
    database = firebase.database()
  },

  countDown: function(i) {
    return new Promise((resolve, reject) => {
      countdownspan.innerText = i
      i--

      if(i>=0) {
        setTimeout(function(){
          BubblePop.countDown(i).then(resolve)
        }, 1000)
      } else {
        resolve('Countdown is resolved')
      }
    })
  },

  animateCoins: function() {
    coinframedesired = Math.floor(Date.now())

    if (currentframe >= coinframes) {
      currentcoinframe = 0;
    } else {
      if(coinframedesired-coinframenow >= coinframerate) {
        currentcoinframe += 1;
        coinframenow = Math.floor(Date.now());

      }
    }
  },

  animateFishes: function() {
    fishframedesired = Math.floor(Date.now())

    if (currentframe >= fishframes) {
      currentframe = 4;
    } else {
      if(fishframedesired-fishframenow >= fishframerate) {
        currentframe += 1;
        fishframenow = Math.floor(Date.now());

      }
    }
  },
  
  animateGame: function() {
    // if player is not lost
    if (!BubblePop.playerLost) {
      requestAnimationFrame(BubblePop.animateGame)
    }

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    BubblePop.animateFishes()
    BubblePop.randomFishDraw()
    for (let i=0; i<animatedFishes.length; i++) {
      animatedFishes[i].update()
    }

    BubblePop.animateCoins()
    BubblePop.randomCoinDraw()
    for (let i=0; i<animatedCoins.length; i++) {
      animatedCoins[i].update()
    }
    
    BubblePop.randomBubbleDraw()
    BubblePop.randomSeaBubbleDraw()
    BubbleManager.iterateBubbles()
    BubbleManager.iterateBlownBubbles()

    for (let i=0; i<water_ripples.length; i++) {
      water_ripples[i].update()
    }

    for (let i=0; i<scores_per_bubble.length; i++) {
      scores_per_bubble[i].update()
    }

    for (let i=0; i<levelUpIndicators.length; i++) {
      levelUpIndicators[i].update()
    }

    BubbleManager.iterateSeaBubbles()
    
    BubblePop.removeUnseenBubbles()
    BubbleManager.removeUnseenBlownBubbles()
    BubblePop.removeUnseenFishes()
    BubblePop.removeRipples()
    BubbleManager.removeBubbleScores()
    BubblePop.removeLevelUpIndicators()
    BubblePop.removeCoins()
    BubbleManager.removeUnseenSeaBubbles()
    
    updateLiteralsOnScreen("Score: ", players_score, 20)
    updateLivesOnScreen("Lives: ", BubblePop.missedBalls, maxBallsMissed, 20)

    BubblePop.renderMissedHits(BubblePop.missedHits)
    
    if (BubblePop.playerLost) {
      losingScene.classList.add('show')
      gameScene.classList.add('lost')
      if (playSounds) {
        gameoverSound.play()
      }
      finalScore.innerText = players_score
      BubblePop.saveScore()
    }

    BubblePop.showHighScore()

  },

  showHighScore: function() {
    if (window.localStorage.getItem('playerlatestscore')) {
      updateLiteralsOnScreen("High score: ", window.localStorage.getItem('playerlatestscore'), 60)
    }
  },

  saveScore: function() {
    if (
        window.localStorage && 
        window.localStorage.getItem('playerlatestscore') < players_score
        ) {
      window.localStorage.setItem('playerlatestscore', players_score)
    }
  },
  
  sendScore: function() {
    if (!BubblePop.sentScoreToLeaderboard) {
      if (nicknameText.value === '') {
        alert('You must enter a nickname in order to post your score')
      } else {
        database.ref('/leaderboard').push({
          nickname: nicknameText.value,
          score: players_score
        }, function(error) {
          if (error) {
            alert('Something went wrong with saving your data in leaderboard...')
          } else {
            BubblePop.sentScoreToLeaderboard = true
            alert('Your score was sent to leaderboard. Gongratulations!')
          }
        })
      }
    } else {
      alert('You have already sent your score.')
    }
  },

  renderMissedHits: function( hits ) {
    updateLiteralsOnScreen("Missed hits: ", hits, 40)
  },

  createRipple: function(x, y, r, opacity) {
    water_ripple = new Ripple(x, y, r, opacity)
    water_ripples.push(water_ripple)
  },

  removeRipples: function() {
    let remainingRipples = water_ripples.filter( ripple => ripple.r < 30 )
    water_ripples = [...remainingRipples]
  },

  removeLevelUpIndicators: function() {
    let remainingLevelUpIndicators = levelUpIndicators.filter( lu => lu.opacity >= 0 )
    levelUpIndicators = [...remainingLevelUpIndicators]
  },

  removeCoins: function() {
    let remainingCoins = animatedCoins.filter( coin => coin.y + coinheight >= 0 && coin.y <= canvas.height )
    animatedCoins = [...remainingCoins]
  },

  blowBubble: function(listenerDevice) {
    let clickX, clickY

    // multiplication needed cause of resizing issues with circles
    if (listenerDevice === CLICKLISTENER) {
      clickX = (event.clientX - offsetX)*scaledX;
      clickY = (event.clientY - offsetY)*scaledY;
    } else {
      clickX = (event.touches[0].clientX - offsetX)*scaledX;
      clickY = (event.touches[0].clientY - offsetY)*scaledY;
    }

    BubblePop.createRipple(clickX, clickY, 0.2, 0.01)
    
    let newBubbles = bubbles.filter( b => {
      let dx = clickX - b.x
      let dy = clickY - b.y
      let dist = Math.abs(Math.sqrt(dx*dx + dy*dy));
      return dist > b.r
    } )
    
    let newCoins = animatedCoins.filter( b => 
      !(clickX <= b.x + coinwidth &&
      clickX >= b.x &&
      clickY <= b.y + coinheight &&
      clickY >= b.y)
    )
      
    if (
      (newCoins.length === animatedCoins.length) &&
      (newBubbles.length === bubbles.length)
      ) {
      players_score -= penalty_score
      
      if (players_score <= 0) {
        players_score = 0
      }

      score_per_bubble = new ScoreDisplayed(`-${penalty_score}`, 9, clickX, clickY, .5, 1, .6, 'red')
      scores_per_bubble.push(score_per_bubble)
      BubblePop.missedHits += 1
      inARow = 0

    } else if (newCoins.length !== animatedCoins.length) {
      if (playSounds) {
        coinSound.play()
      }

      animatedCoins = [...newCoins]
      BubbleManager.generateBlownBubbles(clickX, clickY, 'yellow')
      
      players_score += coinScore
      
      score_per_bubble = new ScoreDisplayed(`+${coinScore}`, 9, clickX, clickY, .5, 1, .6, 'green')
      scores_per_bubble.push(score_per_bubble)

    } else if (newBubbles.length !== bubbles.length) {

      if (playSounds) {
        blop.play()
      }
  
      bubbles = [...newBubbles]
    
      BubbleManager.generateBlownBubbles(clickX, clickY, 'lavender')
      
      players_score += bubble_score
      
      score_per_bubble = new ScoreDisplayed(`+${bubble_score}`, 9, clickX, clickY, .5, 1, .6, 'green')
      scores_per_bubble.push(score_per_bubble)
      inARow += 1
      
      if (inARow === inARowAchievement) {
        clearTimeout(timeout)
        timeout = setTimeout(function() {
          levelUpIndicatorText = new LevelUpIndicator(`${inARowAchievement} IN A ROW!`, -50, 5, 1)
          levelUpIndicators.push(levelUpIndicatorText)
          inARow = 0
          players_score += inARowAchievementScore
          if (playSounds) {
            success.play()
          }
        }, 300)
      }
    } else if (fish_blown_bubble) {
      fish_blown_bubble = false
    }
  },

  updateLevel: function() {
    let quickLevel = quickBubbleLevel

    if (levels.length > 0) {
      if (players_score > levels[0].score) {
        quickLevel = levels[0].generate
        spawnY = levels[0].spawnY
        min_dy = levels[0].min_dy
        max_dy = levels[0].max_dy
        min_dx = levels[0].min_dx
        max_dx = levels[0].max_dx
        bubble_score = levels[0].pointsPerBall
        nextFish = levels[0].nextFish
        quickNextFish = levels[0].quickNextFish
        nextCoin = levels[0].nextCoin
        quickNextCoin = levels[0].quickNextCoin
        coinvelocity = levels[0].coinvelocity
        canvas.style.background = `url('./images/${levels[0].bg}')`

        levelUpIndicatorText = new LevelUpIndicator('LEVEL UP!', -50, 5, 1)
        levelUpIndicators.push(levelUpIndicatorText)

        levels.shift()

        if (playSounds) {
          success.play()
        }
      }
    }

    return quickLevel
  },

  randomCoinDraw: function() {
    if (nextCoin === 0) {
      nextCoin = quickNextCoin
    }
    
    if (nextCoin === quickNextCoin) {
      coin_x = getRandomInt(coinwidth, canvas.width - coinwidth)
      coin_y = getRandomInt(coinheight - 100, coinheight - 120)
      
      animatedCoin = new Coin(coin_x, coin_y, coins, coinwidth, coinheight, getRandomInt(coinvelocity, coinvelocity + 6))
      animatedCoins.push(animatedCoin)
    }
    
    nextCoin -= 1
  },

  randomFishDraw: function() {
    
    if (nextFish === 0) {
      nextFish = quickNextFish
    }
    
    if (nextFish === quickNextFish) {
      fishType = getRandomIntIncludingZero(0, 1)

      if (fishType === 0) {
        fishwidth = 60
        fishheight = 57
      } else {
        fishwidth = 40
        fishheight = 38
      }
      
      fish_x = fishType === 0
        ? getRandomInt(canvas.width + fishwidth + 50, canvas.width + fishwidth + 100)
        : getRandomInt(fishwidth - 70, fishwidth - 100)
      fish_y = getRandomInt(0, canvas.height - fishheight)
      randomDX = getRandomInt(fish_min_dx, fish_max_dx)
      
      animatedFish = new Fish(fish_x, fish_y, randomDX, fishType, fishType === 0 ? fish : fish2, fishwidth, fishheight)
      animatedFishes = [...animatedFishes, animatedFish]
    }
    
    nextFish -= 1
  },

  randomSeaBubbleDraw: function() {
    let bubble_y = canvas.height + 10
    
    if (nextSeaBubble === 0) {
      nextSeaBubble = quickSeaBubble
    }
    
    if (nextSeaBubble === quickSeaBubble) {
      for (let i=0; i<3; i++) {
        sea_bubble = new SeaBubble(getRandomInt(30, canvas.width - 30), bubble_y, getRandomInt(3, 5), getRandomInt(-1, 1), getRandomInt(-1, -2))
        sea_bubbles.push(sea_bubble)
      }
    }
    
    nextSeaBubble -= 1
  },

  randomBubbleDraw: function() {
    let bubble_x = getRandomInt(30, canvas.width - 30)
    let spawnX = bubble_x
    let bubble_y = canvas.height + spawnY
    let bubble_r = getRandomInt(minRadius, maxRadius)
    let distance = getRandomInt(min_distance, max_distance)
    
    let bubble_dx = getRandomInt(min_dx, max_dx)
    let bubble_dy = getRandomInt(min_dy, max_dy)
    let initialDX = bubble_dx
    
    if (nextBubble === 0) {
      nextBubble = quickBubbleLevel
    }
    
    if (nextBubble === quickBubbleLevel) {
      bubble = new Bubble(bubble_x, bubble_y, bubble_r, bubble_dx, bubble_dy, distance, spawnX, initialDX)
      bubbles = [...bubbles, bubble]
    }
    
    quickBubbleLevel = BubblePop.updateLevel()
    nextBubble -= 1
  },

  getLeaderboard: function() {
    database.ref('/leaderboard').once('value', function(data) {
      const dataList = Object.values(data.val()).sort(propComparator('score')).reverse().slice(0, 10)
      leaderboardList.innerHTML = ''
      for (let i=0; i<dataList.length; i++) {
        let el = document.createElement('li')

        let counter = document.createElement('span')
        let counterSpan = document.createTextNode(parseInt(i+1) + '. ')
        counter.appendChild(counterSpan)
        el.appendChild(counter)
        
        let nickname = document.createElement('span')
        let nicknameSpan = document.createTextNode(dataList[i]['nickname'])
        nickname.appendChild(nicknameSpan)
        el.appendChild(nickname)
        
        let scoreEl = document.createElement('span')
        let scoreSpan = document.createTextNode(dataList[i]['score'])
        scoreEl.appendChild(scoreSpan)
        el.appendChild(scoreEl)
        
        leaderboardList.appendChild(el)
      }
      
      leaderboardWrapper.classList.toggle('show')
      leaderboardPopup.classList.toggle('show')
    }, function(error) {
      console.log(error)
    })
  },

  removeUnseenBubbles: function() {
    let remainingBubbles = bubbles.filter( b => b.y + b.r > 0 )
    
    if (bubbles.length > remainingBubbles.length) {
      BubblePop.missedBalls += 1
    }

    bubbles = [...remainingBubbles]

    if (BubblePop.missedBalls === maxBallsMissed) {
      BubblePop.playerLost = true
    }
  },

  removeUnseenFishes: function() {
    let remainingAnimatedFishes = animatedFishes.filter( f => f.type === 0 ? f.x + f.fish_width >= 0 : f.x <= canvas.width )
    animatedFishes = [...remainingAnimatedFishes]
  }
}

// ------------------------------------------------------------
window.onload = () => {
  ws.pause()
  clearTimeout(timeout)
  canvas.style.background = "url('./images/underwater-1.jpg')"

  if (navigator.userAgent.indexOf("Chrome") !== -1) {
    blop.src = './sounds/blop.wav'
    gameoverSound.src = './sounds/gameover.wav'
    success.src = './sounds/success.wav'
    coinSound.src = './sounds/coin.wav'
  } else {
    blop.src = './sounds/blop.mp3'
    gameoverSound.src = './sounds/gameover.mp3'
    success.src = './sounds/success.mp3'
    coinSound.src = './sounds/coin.mp3'
  }

  BubblePop.initFirebase()
  
  startButton.addEventListener('mousedown', function() {
    bootScene.classList.add('hide')
    gameScene.classList.add('show')
    resizeCanvas()
    BubblePop.init()
  }, false)
  
  restartButton.addEventListener('mousedown', function() {
    losingScene.classList.remove('show')
    gameScene.classList.remove('lost')
    resizeCanvas()
    BubblePop.resetState()
    BubblePop.init()
  }, false)
  
  startButton.addEventListener('touchstart', function(e) {
    e.preventDefault()
    bootScene.classList.add('hide')
    gameScene.classList.add('show')
    resizeCanvas()
    BubblePop.init()
  }, false)
  
  restartButton.addEventListener('touchstart', function(e) {
    e.preventDefault()
    losingScene.classList.remove('show')
    gameScene.classList.remove('lost')
    resizeCanvas()
    BubblePop.resetState()
    BubblePop.init()
  }, false)
  
  leaderboardButton.addEventListener('mousedown', function() {
    BubblePop.getLeaderboard()
  }, false)
  
  leaderboardButton.addEventListener('touchstart', function(e) {
    e.preventDefault()
    BubblePop.getLeaderboard()
  }, false)
  
  closeLeaderboardPopup.addEventListener('mousedown', function() {
    leaderboardWrapper.classList.remove('show')
    leaderboardPopup.classList.remove('show')
  }, false)
  
  closeLeaderboardPopup.addEventListener('touchstart', function() {
    leaderboardWrapper.classList.remove('show')
    leaderboardPopup.classList.remove('show')
  }, false)
  
  sendScore.addEventListener('mousedown', function() {
    BubblePop.sendScore()
  }, false)
  
  sendScore.addEventListener('touchstart', function(e) {
    e.preventDefault();
    BubblePop.sendScore()
  }, false)
  
  backButton.addEventListener('mousedown', function() {
    losingScene.classList.remove('show')
    bootScene.classList.remove('hide')
    bootScene.classList.add('show')
    gameScene.classList.remove('show')
    gameScene.classList.remove('lost')
    gameScene.classList.add('hide')
    BubblePop.resetState()
  }, false)
  
  backButton.addEventListener('touchstart', function(e) {
    e.preventDefault()
    losingScene.classList.remove('show')
    bootScene.classList.remove('hide')
    bootScene.classList.add('show')
    gameScene.classList.remove('show')
    gameScene.classList.remove('lost')
    gameScene.classList.add('hide')
    BubblePop.resetState()
  }, false)
  
  muteButton.addEventListener('mousedown', function(e) {
    if (e.target.classList.contains('fa-volume-up')) {
      e.target.parentNode.classList.toggle('mute-volume')
    } else {
      e.target.classList.toggle('mute-volume')
    }
    
    playSounds = !playSounds
    
    if (!playSounds) {
      ws.pause()
    } else {
      ws.play()
    }
  }, false)
  
  muteButton.addEventListener('touchstart', function(e) {
    e.preventDefault()
    if (e.target.classList.contains('fa-volume-up')) {
      e.target.parentNode.classList.toggle('mute-volume')
    } else {
      e.target.classList.toggle('mute-volume')
    }
    
    playSounds = !playSounds

    if (!playSounds) {
      ws.pause()
    } else {
      setTimeout(function() {
        ws.play()
      }, 1000)
    }
  }, false)
  
  loadingWrapper.classList.add('hide')
  BubbleManager.resizeBubbles()
    
}

canvas.addEventListener('mousedown', function() {
  BubblePop.blowBubble(CLICKLISTENER)
},
false)

canvas.addEventListener('touchstart', function(e) {
  e.preventDefault()
  BubblePop.blowBubble(TOUCHLISTENER)
},
false)

canvas.addEventListener('touchend', function(e) {
  e.preventDefault()
},
false)


window.addEventListener('resize', function() {
  resizeCanvas()
  BubbleManager.resizeBubbles()
});

ws.addEventListener('ended', function() {
  this.currentTime = 0;
  this.play();
}, false);

