import { BubbleManager } from './BubbleManager'

export default class Bubble {
  constructor(x, y, r, dx, dy, horizontal_distance, spawnX, initialDX) {
    this.x = x // top left x
    this.y = y // top left y
    this.r = r
    this.dx = dx // x velocity
    this.dy = dy // y velocity
    this.horizontal_distance = horizontal_distance
    this.spawnX = spawnX
    this.initialDX = initialDX
  }

  hasCollided(collided_object) {
    for (let i=0; i<collided_object.length; i++) {
      let distX = Math.abs(this.x - collided_object[i].x-collided_object[i].fish_width/2)
      let distY = Math.abs(this.y - collided_object[i].y-collided_object[i].fish_height/2)
      let dx = distX - collided_object[i].fish_width/2
      let dy = distY - collided_object[i].fish_height/2

      // COLLISION DETECTION!!!
      if ( dx*dx + dy*dy <= (this.r*this.r) ) {
        BubbleManager.handleCollision(this.x, this.y, dx, dy)
        
        fish_blown_bubble = true
      } else {
        fish_blown_bubble = false
      }
    }
  }

  draw() {
    ctx.beginPath();
    ctx.fillStyle = "rgba(231, 252, 255, 0.8)";
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI, true);
    ctx.fill();
  }

  update(collided_object) {

    if (
          (this.x <= this.spawnX - (parseInt(this.horizontal_distance / 2))) ||
          (this.x >= this.spawnX + (parseInt(this.horizontal_distance / 2)))
        ) {
      
          if (this.x <= this.spawnX - (parseInt(this.horizontal_distance / 2))) {
            this.dx += .04
          }

          if (this.x >= this.spawnX + (parseInt(this.horizontal_distance / 2))) {
            this.dx -= .04
          }

          if (Math.abs(this.dx) === this.initialDX) {
            this.dx = this.initialDX
          }
    }

    this.x += this.dx

    this.y -= this.dy

    this.draw()

    this.hasCollided(collided_object)
  }
}