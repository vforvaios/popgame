export const getRandomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)

  let num = Math.floor(Math.random() * (max - min + 1)) + min
  return (num === 0) ? getRandomInt(min, max) : num
}

export const getRandomIntIncludingZero = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)

  let num = Math.floor(Math.random() * (max - min + 1)) + min
  return num
}

export const resizeCanvas = () => {

  let canvasWidth = canvas.width;
  let canvasHeight = canvas.height;
  let desiredHeight = window.innerHeight;
  let ratio = canvasWidth / canvasHeight;

  let desiredWidth = desiredHeight * ratio;

  canvas.style.width = desiredWidth + "px";
  canvas.style.height = desiredHeight + "px";
  offsetX = canvas.offsetLeft;
  offsetY = canvas.offsetTop;

  // needed everytime for scaling purposes due to resizing canvas
  scaledX = canvas.width / canvas.offsetWidth;
  scaledY = canvas.height / canvas.offsetHeight;
}

export const updateLiteralsOnScreen = (text, value, yposition) => {
  ctx.beginPath()
  ctx.font = "13px 'Open Sans', sans-serif"
  ctx.strokeStyle = 'black'
  ctx.lineWidth = 4
  ctx.textAlign = 'left'
  ctx.strokeText(text + value, 10, yposition)
  ctx.fillStyle = "white"
  ctx.textAlign = 'left'
  ctx.fillText(text + value, 10, yposition)
  ctx.closePath()
}

export const updateLivesOnScreen = (text, missedBalls, maxBallsAllowedToMiss, yposition) => {
  ctx.beginPath()
  ctx.font = "13px 'Open Sans', sans-serif"
  ctx.strokeStyle = 'black'
  ctx.lineWidth = 4
  ctx.textAlign = 'left'
  ctx.strokeText(text, canvas.width - (maxBallsAllowedToMiss*20) - 70, yposition)
  ctx.fillStyle = "white"
  ctx.textAlign = 'left'
  ctx.fillText(text, canvas.width - (maxBallsAllowedToMiss*20) - 70, yposition)
  ctx.closePath()
  
  for (let i=0; i< maxBallsAllowedToMiss; i++) {
    ctx.beginPath()
    ctx.arc(canvas.width - 18 -22*i, yposition - 5, 8, 0, 2 * Math.PI)
    ctx.strokeStyle = 'white'
    ctx.lineWidth = 2
    ctx.stroke()
    ctx.fillStyle = 'green'
    switch(missedBalls) {
      case 1:
        if (i===0) { ctx.fillStyle = 'red' }
        break
      case 2:
        if (i===0 || i===1) { ctx.fillStyle = 'red' }
        break
      case 3:
        ctx.fillStyle = 'red'
        break
      default:
        ctx.fillStyle = 'green'
    }
    ctx.fill()
    ctx.closePath()
  }
}

export const firebaseConfig = {
  apiKey: "AIzaSyBem8XbUTeGug3px2Adzmn_GeQhTss0gQ4",
  authDomain: "bubblepop-149ce.firebaseapp.com",
  databaseURL: "https://bubblepop-149ce.firebaseio.com",
  projectId: "bubblepop-149ce",
  storageBucket: "bubblepop-149ce.appspot.com",
  messagingSenderId: "17140369565",
  appId: "1:17140369565:web:4d80f1dc292ac5f2"
}

export const propComparator = propName =>
  (a, b) => a[propName] === b[propName] ? 0 : a[propName] < b[propName] ? -1 : 1