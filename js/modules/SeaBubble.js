export default class SeaBubble {
  constructor(x, y, r, dx, dy) {
    this.x = x
    this.y = y
    this.r = r
    this.dx = dx
    this.dy = dy
  }

  draw() {
    ctx.beginPath()
    ctx.strokeStyle = 'lavender'
    ctx.lineWidth = 1
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI, true)
    ctx.stroke()
    ctx.closePath()
  }

  update() {
    this.x += this.dx

    this.dy *= 1.09;// friction
    this.y += this.dy;

    this.draw() 
  }
}