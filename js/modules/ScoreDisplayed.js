export default class ScoreDisplayed {
  constructor(score, fontsize, x, y, dx, dy, opacity, type) {
    this.score = score
    this.fontsize = fontsize
    this.x = x
    this.y = y
    this.dx = dx
    this.dy = dy
    this.opacity = opacity 
    this.type = type
  }

  draw() {
    ctx.beginPath()
    ctx.font = `bold ${this.fontsize}px 'Open Sans', sans-serif`
    ctx.strokeStyle = `rgba(255, 255, 255, ${this.opacity}`
    ctx.lineWidth = 2
    ctx.strokeText(this.score, this.x, this.y)
    if (this.type === 'green') {
      ctx.fillStyle = `rgba(9, 146, 9, ${this.opacity}`
    } else {
      ctx.fillStyle = `rgba(255, 0, 0, ${this.opacity}`
    }
    ctx.fillText(this.score, this.x, this.y)
    
  }

  update() {
    this.x += this.dx
    this.y -= this.dy

    if (this.fontsize < 22) {
      this.opacity += .1
      this.fontsize += .5
    } else {
      this.opacity -= .25
    }

    this.draw()
  }
}