export default class LevelUpIndicator {
  
  constructor(textToDisplay, y, dy, opacity) {
    this.y = y
    this.dy = dy
    this.opacity = opacity
    this.textToDisplay = textToDisplay
  }

  draw() {
    ctx.beginPath()
    ctx.font = `bold 30px 'Luckiest Guy', sans-serif`
    ctx.fillStyle = `rgba(22, 216, 22, ${this.opacity})`
    ctx.textAlign = 'center'
    ctx.fillText(this.textToDisplay, parseInt(parseInt(canvas.width)/2), this.y)
    ctx.closePath()
  }

  update() {
    if (this.y >= parseInt(parseInt(canvas.style.height)/2)) {
      this.opacity -= .02
    }
    
    this.y += this.dy

    this.draw()
  }
}